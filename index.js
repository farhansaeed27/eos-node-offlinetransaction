let { Api, JsonRpc } = require("eosjs");
let { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig");
let fetch = require("node-fetch");
const { TextDecoder, TextEncoder,inspect } = require("util");
const fs = require("fs");
const path = require("path");
let moment = require("moment");
let rawTransaction=fs.readFileSync(__dirname+"/eos_transaction.json").toString()
rawTransaction=JSON.parse(rawTransaction)

const privateKeys = ["5KHUaS3dJJVxN1gKyicrG9NqNib3uUdAai9jzm1z4U5cQLmZaN9"];
const signatureProvider = new JsSignatureProvider(privateKeys);
const rpc = new JsonRpc("https://api.eosrio.io", { fetch });
let account = "rnssolution2";

async function getNetworkInfo() {
  let info = await rpc.get_info();
  let result = await rpc.get_block(info.last_irreversible_block_num);
  return {
    chain_id: info.chain_id,
    block_num: result.block_num,
    ref_block_prefix: result.ref_block_prefix
  };
}

getNetworkInfo().then(async info => {
  let expiration = moment()
    .add(20, "minutes")
    .toDate()
    .toString();

  const api = new Api({
    rpc,
    signatureProvider,
    chainId: info.chain_id,
    textDecoder: new TextDecoder(),
    textEncoder: new TextEncoder()
  });

  rawTransaction={...rawTransaction,expiration,ref_block_num:info.block_num & 0xffff,ref_block_prefix:info.ref_block_prefix}
  rawTransaction.actions[0].authorization[0].actor=account
  rawTransaction.actions[0].data={from:account,to:account,quantity:"1000",memo:""}
  
  let result = await api.transact(rawTransaction, {
    blocksBehind: 3,
    broadcast: false,
    sign: true,
    expireSeconds: 60 * 60
  });

  console.log({ serializedTransaction:Buffer.from(
    result.serializedTransaction
  ).toString("hex"), signatures:result.signatures });

});
